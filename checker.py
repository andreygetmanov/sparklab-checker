import re


def undesirable_libs_imported(script):
    """ Checks if the script imports undesirable libraries. """

    undesirable_libs = set(['os', 'sys', 'subprocess', 'multiprocessing', 'command'])
    modules_imported = set()
    with open(script, 'r') as opened_file:
        file_lines = opened_file.readlines()

        for line in file_lines:

            # import XXX
            match = re.match(r'import\s([\w\.]+)', line)
            if match:
                modules_imported.add(match.groups()[0])

            # from XXX
            match = re.match(r'from\s([\w\.]+)', line)
            if match:
                modules_imported.add(match.groups()[0])

    return len(undesirable_libs.intersection(modules_imported)) > 0


def line_without_comment(line):
    """ Cuts the comment part from a line. """

    if '#' in line:
        return line.split('#')[0]
    if '"""' in line:
        return line.split('"""')[0]
    if "'''" in line:
        return line.split("'''")[0]
    return line


def undesirable_operations_used(script):
    """ Checks if the script uses undesirable operations. """

    undesirable_operations = set(['spark', 'SparkSession', 'read', 'write', 'open'])
    with open(script, 'r') as opened_file:
        file_lines = opened_file.readlines()

        for line in file_lines:
            if any(operation in line_without_comment(line) for operation in undesirable_operations):
                return True

    return False


script = 'spark_lab.py'


try:
    assert undesirable_libs_imported(script) is False
except AssertionError:
    print('Error: Undesirable libraries are imported')

try:
    assert undesirable_operations_used(script) is False
except AssertionError:
    print('Error: Undesirable operations are used')
